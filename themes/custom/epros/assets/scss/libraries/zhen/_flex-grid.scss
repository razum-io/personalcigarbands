@charset "UTF-8";

/*
 * Container
 * use: @include zhen--container({?gutter: px}, {?gutter-method: string});
 * parameters:
 *   $gutter        - indents on the sides
 *                    required parameter
 *                    default: 20px
 *                    possible values: any value in px
 *   $gutter-method - type of indents
 *                    required parameter
 *                    default: padding
 *                    possible values: padding|margin
 */
@mixin zhen--container(
  $gutter:        $zhen-gutter-container,
  $gutter-method: $zhen-gutter-method
) {
  box-sizing:              border-box;
  #{$gutter-method}-right: $gutter;
  #{$gutter-method}-left:  $gutter;
  @if $gutter-method == padding {
    margin-right: auto;
    margin-left:  auto;
  }
}

/*
 * Row
 * use: @include zhen--row({?direction: string}, {?gutter: px}, {?gutter-method: string});
 * parameters:
 *   $direction     - direction of flex
 *                    required parameter
 *                    default: row
 *                    possible values: row|column
 *   $gutter        - indents on the sides
 *                    required parameter
 *                    default: 10px
 *                    possible values: any value in px
 *   $gutter-method - type of indents
 *                    required parameter
 *                    default: padding
 *                    possible values: padding|margin
 */
@mixin zhen--row(
  $direction:     $zhen-direction,
  $gutter:        $zhen-gutter-row,
  $gutter-method: $zhen-gutter-method
) {
  display:                 flex;
  flex-direction:          $direction;
  flex-wrap:               wrap;
  box-sizing:              border-box;
  #{$gutter-method}-right: $gutter;
  #{$gutter-method}-left:  $gutter;
}

/*
 * Auto sizing columns
 * The element with this class must be inside the element with class zhen--row
 * use: @include zhen--column-auto({?gutter: px}, {?gutter-method: string});
 * alias: @include zhen--col-auto({?gutter: px}, {?gutter-method: string});
 * parameters:
 *   $gutter        - indents on the sides
 *                    required parameter
 *                    default: 5px
 *                    possible values: any value in px
 *   $gutter-method - type of indents
 *                    required parameter
 *                    default: padding
 *                    possible values: padding|margin
 */
@mixin zhen--column-auto(
  $gutter:        $zhen-gutter-column,
  $gutter-method: $zhen-gutter-method
) {
  flex-grow:               1;
  flex-basis:              0;
  max-width:               100%;
  #{$gutter-method}-right: $gutter;
  #{$gutter-method}-left:  $gutter;
}
/* alias of zhen--column-auto() */
@mixin zhen--col-auto(
  $gutter:        $zhen-gutter-column,
  $gutter-method: $zhen-gutter-method
) {
  @include zhen--column-auto($gutter, $gutter-method);
}

/*
 * Ranged columns
 * The element with this class must be inside the element with class zhen--row
 * use: @include zhen--column({!span: int}, {?offset: int}, {?columns: int}, {?safe: bool}, {?gutter: px}, {?gutter-method: string});
 * alias: @include zhen--col({!span: int}, {?offset: int}, {?columns: int}, {?safe: bool}, {?gutter: px}, {?gutter-method: string});
 * parameters:
 *   $span          - occupied space
 *                    required parameter
 *                    possible values: from 1 to $zhen-columns (default: 12)
 *   $offset        - offset
 *                    optional parameter
 *                    possible values: any int
 *                    max correct value: from 1 to $zhen-columns - $span
 *   $columns       - number of columns in this row
 *                    optional parameter
 *                    default: 12
 *                    possible values: any int
 *   $safe          - safe mod
 *                    optional parameter
 *                    if it is enabled
 *                     and if the amount of the indent and width of the element exceeds the number of columns
 *                     then the width of the element is automatically shortened
 *                    possible values: true|false
 *   $gutter        - indents on the sides
 *                    optional parameter
 *                    default: 5px
 *                    possible values: any value in px
 *   $gutter-method - type of indents
 *                    optional parameter
 *                    default: padding
 *                    possible values: padding|margin
 */
@mixin zhen--column(
  $span,
  $offset:        null,
  $columns:       $zhen-columns,
  $safe:          $zhen-safe,
  $gutter:        $zhen-gutter-column,
  $gutter-method: $zhen-gutter-method
) {
  @if $span == 0 {
    $span: 1;
  }
  box-sizing:              border-box;
  flex-grow:               0;
  flex-shrink:             0;
  #{$gutter-method}-right: $gutter;
  #{$gutter-method}-left:  $gutter;
  flex-basis:              ((100% / $columns) * $span);
  max-width:               ((100% / $columns) * $span);
  @if $offset {
    @if $safe and $span > $columns {
      $span: $columns;
    }
    @if $safe and $span + $offset > $columns {
      $offset: $columns - $span;
    }

    $span-offset: ((100% / $columns) * 1);
    margin-left: $span-offset * $offset;
  }
}
/* alias of zhen--column() */
@mixin zhen--col(
  $span,
  $offset:        null,
  $columns:       $zhen-columns,
  $safe:          $zhen-safe,
  $gutter:        $zhen-gutter-column,
  $gutter-method: $zhen-gutter-method
) {
  @include zhen--column($span, $offset, $columns, $safe, $gutter, $gutter-method);
}

/* HORIZONTAL ALIGNMENT */

/*
 * Horizontal alignment for inside elements from start of the Row
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--start();
 */
@mixin zhen--start() {
  justify-content: flex-start;
  text-align:      start;
}

/*
 * Horizontal alignment for inside elements from center of the Row
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--center();
 */
@mixin zhen--center() {
  justify-content: center;
  text-align:      center;
}

/*
 * Horizontal alignment for inside elements from end of the Row
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--end();
 */
@mixin zhen--end() {
  justify-content: flex-end;
  text-align:      end;
}

/* VERTICAL ALIGNMENT BY PARENT */

/*
 * Vertical alignment for inside elements from top of the Row
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--top();
 */
@mixin zhen--top() {
  align-items: flex-start;
}

/*
 * Vertical alignment for inside elements from middle of the Row
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--middle();
 */
@mixin zhen--middle() {
  align-items: center;
}

/*
 * Vertical alignment for inside elements from bottom of the Row
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--bottom();
 * alias: @include zhen--bot();
 */
@mixin zhen--bottom() {
  align-items: flex-end;
}
/* alias of zhen--bottom() */
@mixin zhen--bot() {
  @include zhen--bottom();
}

/* VERTICAL ALIGNMENT BY CHILD */

/*
 * Vertical alignment for element inside Row by top
 * The element with this class must be on the same level as the element with class zhen--column or zhen--column-auto
 * use: @include zhen--top-self();
 */
@mixin zhen--top-self() {
  align-self: flex-start;
}

/*
 * Vertical alignment for element inside Row by middle
 * The element with this class must be on the same level as the element with class zhen--column or zhen--column-auto
 * use: @include zhen--middle-self();
 */
@mixin zhen--middle-self() {
  align-self: center;
}

/*
 * Vertical alignment for element inside Row by bottom
 * The element with this class must be on the same level as the element with class zhen--column or zhen--column-auto
 * use: @include zhen--bottom-self();
 * alias: @include zhen--bot-self();
 */
@mixin zhen--bottom-self() {
  align-self: flex-end;
}
/* alias of zhen--bottom-self() */
@mixin zhen--bot-self() {
  @include zhen--bottom-self();
}

/* DISTRIBUTION */

/*
 * Distribution of the elements inside Row with value - around
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--around();
 * alias: @include zhen--row-around();
 * parameters:
 *   $withRow       - along with Row
 *                    optional parameter
 *                    default value: false
 *                    possible values: false|true
 *   $direction     - direction of flex
 *                    required parameter
 *                    default: row
 *                    possible values: row|column
 *   $gutter        - indents on the sides
 *                    required parameter
 *                    default: 10px
 *                    possible values: any value in px
 *   $gutter-method - type of indents
 *                    required parameter
 *                    default: padding
 *                    possible values: padding|margin
 */
@mixin zhen--around(
  $withRow:       false,
  $direction:     $zhen-direction,
  $gutter:        $zhen-gutter-row,
  $gutter-method: $zhen-gutter-method
) {
  @if $withRow {
    @include zhen--row($direction, $gutter, $gutter-method);
  }
  justify-content: space-around;
}
/* alias of zhen--around(true) */
@mixin zhen--row-around(
  $direction:     $zhen-direction,
  $gutter:        $zhen-gutter-row,
  $gutter-method: $zhen-gutter-method
) {
  @include zhen--around(true, $direction, $gutter, $gutter-method);
}

/*
 * Distribution of the elements inside Row with value - between
 * The element with this class must be on the same level as the element with class zhen--row
 * use: @include zhen--between();
 * alias: @include zhen--row-between();
 * parameters:
 *   $withRow       - along with Row
 *                    optional parameter
 *                    default value: false
 *                    possible values: false|true
 *   $direction     - direction of flex
 *                    required parameter
 *                    default: row
 *                    possible values: row|column
 *   $gutter        - indents on the sides
 *                    required parameter
 *                    default: 10px
 *                    possible values: any value in px
 *   $gutter-method - type of indents
 *                    required parameter
 *                    default: padding
 *                    possible values: padding|margin
 */
@mixin zhen--between(
  $withRow:       false,
  $direction:     $zhen-direction,
  $gutter:        $zhen-gutter-row,
  $gutter-method: $zhen-gutter-method
) {
  @if $withRow {
    @include zhen--row($direction, $gutter, $gutter-method);
  }
  justify-content: space-between;
}
/* alias of zhen--between(true) */
@mixin zhen--row-between(
  $direction:     $zhen-direction,
  $gutter:        $zhen-gutter-row,
  $gutter-method: $zhen-gutter-method
) {
  @include zhen--between(true, $direction, $gutter, $gutter-method);
}


/* ORDERING */

/*
 * Puts the element at the beginning of the Row
 * The element with this class must be on the same level as the element with class zhen--column or zhen--column-auto
 * use: @include zhen--first();
 */
@mixin zhen--first() {
  order: -1;
}

/*
 * Puts the element at the end of the Row
 * The element with this class must be on the same level as the element with class zhen--column or zhen--column-auto
 * use: @include zhen--last();
 */
@mixin zhen--last() {
  order: 1;
}

/*
 * Sets a specific ordering number for the Column
 * The element with this class must be on the same level as the element with class zhen--column or zhen--column-auto
 * use: @include zhen--order({!order: int});
 * parameters:
 *   $span - order of item
 *           required parameter
 *           possible values: any int
 */
@mixin zhen--order(
  $order
) {
  order: $order;
}

/* REVERSING */

/*
 * Sets the reverse order for the elements in the Row
 * use: @include zhen--reverse({direction: string});
 * parameters:
 *   $direction - direction for reverse
 *                required parameter
 *                default: row
 *                possible values: row|column
 */
@mixin zhen--reverse(
  $direction: $zhen-reverse-direction
) {
  @if $direction == row {
    flex-direction: row-reverse;
  } @else {
    flex-direction: column-reverse;
  }
}
